## 课后问答

问： 老师，刚可能没注意听到，那后面是要安装一下rbase 吗？  
答： graphics包是R的一部分，安装好的R，就是R-base。

问： 刚才电脑里install grapics，一直提示失败  
答： 首先，包的名称应该是graphics。其次，安裝了R，graphics包已經自動安裝好了，就可以直接繪圖了，

问： 张老师，有关函数传递那里没懂，有劳释疑下  
答： 之所以现在讲参数传递，是因为绘图相关的函数往往有很多参数，这些函数的参数大部分都与`plot.default`的参数有关。调用这些函数时，一般会修改参数，而修改这些函数的值，其实是修改函数内部所调用的`plot.default`函数的参数的值。不难想象，如果在定义一个调用`plot.default`函数的函数时，都要将`plot.default`的函数的参数全部重新列出来将会非常麻烦，此时，就可以用`...`传递参数。`plot.default`的函数再多，也没有关系，只要用`...`就可以代替。

更抽象一些，假设有函数A，函数A在定义时调用了函数B。函数A有若干参数，函数B也有若干参数。要将函数B的参数传递给A，就是在定义函数A时，以`...`来表示函数B的一个或者多个参数，并将函数A内部调用的函数B的部分参数也用`...`表示，此时函数B的参数就可以在调用的时候传递给函数A。

例1：

```R
plot222 <- function(x, ...){
    plot.default(x, ...)
}

plot222(cars, main = "Cars",
    xlab = "Speed",
    ylab = "Distance")
```

`plot222` 函数并没有定义 `main`, `xlab`, `ylab` 等参数，但是可以通过参数传递直接调用 `plot.default` 函数中的参数。

例2：

```R
## S3 method for class 'formula'
barplot(formula, data, subset, na.action,
        horiz = FALSE, xlab = NULL, ylab = NULL, ...)
```

`barplot`参数列表中的`...`，就保证`barplot`函数可以调用`plot.default`中的参数。


问： 如果一个函数里有两个调用的函数有`...`会发生什么？

答： 这就取决于你调用的参数是来自哪个函数。如果两个函数都有同一个参数，那么就都会被调用。但是有一点要注意，定义函数时，不能同时出现两个`...`。


问： 老师您好，我有个问题，就是图片保存大小的问题。尤其是用plot 区出图用export 手动输出的时候，图形区域极其容易变形，就是字体大小会变。  

答： 这个问题暂时没有办法回答。我建议提供1. R以及程序包的版本。2.代码和数据。3. 讲清楚字体大小会变究竟是什么，是相比什么会变呢？

问： 老师，请问这两次课程有实操红包吗？

答： 如果在精力允许的情况下，是会录制一些操作视频的。 

问： svg是什么格式？会用到这样的图吗？

答： SVG 意为可缩放矢量图形（Scalable Vector Graphics），使用 XML 格式定义图像，一般通过浏览器渲染。可以用Adobe Illustrator编辑。

问： 用`par`函数更改了设置，不重启，如何恢复默认设置呢

答： 一般是先将`par`的各参数保存到一个list中，命令为`oldpar <- par()`

如果要恢复到默认设置，就用 `par(oldpar)` 即可。

开启某一个设备之后，`par`的修改只对该设备有效，关闭该设备，`par`参数就恢复到默认设置。


问： 请问，视频可否回放？

答： 视频、幻灯片全部都在BioONE和bilibili回放。请关注helloBD公众号的相关内容。

问： B站up主名叫什么呢？

答： https://space.bilibili.com/1979221372， 搜索 HelloBD 即可。


问： 我想改字体，比如说calibri, 我老调不了legend的字体

答： 可以试试`legend`函数的`text.font`参数。

问： 用`help`查询函数的用法，看不懂提供的函数括号里的内容，该怎么解决这个问题？

答： 函数括号里的内容，就是函数的参数列表。正常的R函数帮助文件，在Usage之后，都有一个Arguments的部分，详细介绍各参数的格式以及意义。


问： 请教张老师一个非绘图的问题。想知道如果有十个样本，分别统计了不同物种，但只显示个体数不为0物种。有什么办法让每个样本都显示每一个物种，其中未统计到的为0吗？想问下有没有解决这个问题的函数。感谢。

答： 知道你最终想得到的是样方-物种矩阵，示例代码如下：

```R
species <- paste0("sp",1:20)
nsp = sample(5:10, 5)

# 生成模拟数据。注意，此处plot为样方
plot1 <- data.frame(SPECIES = sample(species, nsp[1]), coverage = runif(nsp[1]))
plot2 <- data.frame(SPECIES = sample(species, nsp[2]), coverage = runif(nsp[2]))
plot3 <- data.frame(SPECIES = sample(species, nsp[3]), coverage = runif(nsp[3]))
plot4 <- data.frame(SPECIES = sample(species, nsp[4]), coverage = runif(nsp[4]))
plot5 <- data.frame(SPECIES = sample(species, nsp[5]), coverage = runif(nsp[5]))

plot_names <- c("plot1", "plot2", "plot3", "plot4", "plot5")

plot_dat <- data.frame(plot = rep(plot_names, nsp), rbind(plot1, plot2, plot3, plot4, plot5))

library(picante)
sample2matrix(plot_dat[,c("plot", "coverage", "SPECIES")])
```

问： ggplot2的图能不能和R-base混用？

答：不能直接混用。

但是，余光创教授的ggplotify程序包（https://github.com/GuangchuangYu/ggplotify）可以将R-base绘图代码转换为grid包的grob对象，grob对象则可以十分方便地放入ggplot2中。

另外，ggthemes包有一个叫做`theme_base`的主题，生成的图形模仿R base的风格，如果实在喜欢R-base的风格，用ggplot2加这个主题也是可以绘制出来的。示例代码如下：

```R
library(ggplot2)
library(ggthemes)
p <- ggplot(mtcars) + geom_point(aes(x = wt, y = mpg,
  colour = factor(gear))) + facet_wrap(~am)
p + theme_base()
```

